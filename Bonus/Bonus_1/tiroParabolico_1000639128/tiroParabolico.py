import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico:
    '''
    Simulador tiro parabólico.

    Atributos
    --------
    angle : float
        ángulo inicial de tiro respecto al eje x, en radianes
    v0 : float
        magnitud de la velocidad inicial
    x0 : float
        posición inicial en x
    height : float
        altura inicial
    g : float
        valor de la aceleración gravitacional.
    '''
    
    def __init__(self,angle: float,v: float,pos: list,g = 9.8): 
        '''
        Parámetros
        ----------
        angle : float
            Ángulo inicial de tiro respecto al eje x, debe estar en radianes.
        v : float
            magnitud velocidad inicial
        pos : list
            {list} posición inicial [x0,y0]
        g : float
            Valor de la gravedad, por defecto 9.8
        '''
        self.angle = angle
        self.v0 = v
        self.x0, self.height = pos
        self.g = g
    
    def velocityX(self):
        '''
        Retorna el valor de la componente "x" de la velocidad, la cual es constante.
        '''
        return self.v0*np.cos(self.angle)
    
    def velocityY(self,time):
        '''
        Retorna el valor de la componente "y" de la velocidad despues de un cierto tiempo.

        Parámetros
        -----
        time:
            tiempo luego del lanzamiento, puede ser float o array de numpy, velocitY(0) retorna la      velocidad inicial en y
        '''
        return self.v0*np.sin(self.angle)-self.g*time
    
    def flyTime(self):
        '''
        Retorna el tiempo de vuelo, definido como el tiempo que el proyectil se mantiene en el aire antes
        de chocar con el suelo (y=0).
        '''
        return (self.velocityY(0)+np.sqrt(self.velocityY(0)**2+2*self.g*self.height))/self.g
    
    def apex(self):
        '''
        Retorna una tupla con el punto máximo de altura y el tiempo de altura máxima.
        '''
        if self.velocityY(0)<0:
            print("El movimiento no tiene punto máximo debido a que la velocidad inicial es negativa")
            return None
        else:
            apexTime = self.velocityY(0)/self.g
            return self.height+self.velocityY(0)*apexTime-self.g*apexTime**2/2, apexTime
    
    def drawTraject(self,time,n = 1000):
        '''
        Guarda una imagen traject.png con la trayectoria del a particula durante un tiempo.

        Parámetros
        ----------
        time:
            Tiempo de duración de la simulación
        n:
            número de puntos a graficar
        '''
        self.time = np.linspace(0,time,n)

        plt.figure(dpi = 150)
        #Usa las ecuaciones de movimiento para graficar la trayectoria
        plt.plot(self.x0 +self.velocityX()*self.time,
                 self.height+self.velocityY(0)*self.time-self.g*self.time**2/2)

        plt.axhline(color = 'k')
        plt.title('Trajectoria de tiro')
        plt.xlabel('x')
        plt.ylabel('y')

        #Para comprobar que el valor del tiempo de vuelo es correcto según la gráfica
        flytime_point = (self.x0+self.flyTime()*self.velocityX(),0)
        plt.annotate(f'flyTime={self.flyTime():.2f}',xy = flytime_point)

        plt.savefig("traject.png")
