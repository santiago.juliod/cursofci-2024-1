import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico:
    """
    Clase que modela un tiro parabólico en dos dimensiones.
    
    Atributos:
        x_inicial (float): Posición inicial en el eje x.
        y_inicial (float): Posición inicial en el eje y.
        v_inicial (float): Magnitud de la velocidad inicial.
        angulo (float): Ángulo de lanzamiento en radianes.
        gravedad (float): Aceleración debida a la gravedad.
    """
    def __init__(self, x0 = 0, y0 = 0, v0 = 0, angulo = 0, g = 9.8):
        self.x_inicial = x0
        self.y_inicial = y0
        self.v_inicial = v0
        self.angulo = np.radians(angulo)  # Convertimos el ángulo a radianes
        self.gravedad = g

    def vel_x(self):
        """
        Calcula la componente x de la velocidad inicial.

        Return:
            float: Velocidad inicial en dirección x.
        """
        return self.v_inicial * np.cos(self.angulo)

    def vel_y(self):
        """
        Calcula la componente y de la velocidad inicial.

        Return:
            float: Velocidad inicial en dirección y.
        """
        return self.v_inicial * np.sin(self.angulo)

    def tiempo_max(self):
        """
        Calcula el tiempo máximo de vuelo del proyectil.

        Return:
            float: Tiempo máximo de vuelo.
        """
        return (2 * self.vel_y()) / self.gravedad #Según las ecuaciones del MUA

class plotTiroParabolico:
    """
    Clase que grafica la trayectoria de un tiro parabólico.
    
    Atributos:
        tiro_parabolico (tiroParabolico): Objeto que representa el tiro parabólico.
    """
    def __init__(self, tp):
        """
        Grafica la trayectoria del proyectil.
        """
        self.tiro_parabolico = tp

    def plot(self):
        t_total = self.tiro_parabolico.tiempo_max() #El tiempo total de graficación es el tiempo máx
        t = np.linspace(0, t_total, 100) #Se crea el array de tiempos
        #Se definen las componentes horizontal y vertical de la posición
        x = self.tiro_parabolico.x_inicial + self.tiro_parabolico.vel_x() * t
        y = self.tiro_parabolico.y_inicial + self.tiro_parabolico.vel_y() * t - 0.5 * self.tiro_parabolico.gravedad * t**2
        
        #Graficación
        plt.figure(figsize=(8, 12))
        plt.plot(x, y, 'k', linewidth=2)
        plt.xlabel('Posición en x (m)', fontsize=12)
        plt.ylabel('Posición en y (m)', fontsize=12)
        plt.title('Trayectoria del proyectil', fontsize=14)
        plt.grid(True, linestyle='--', alpha=0.7)
        plt.legend(fontsize=10)
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)

        plt.tight_layout()
        plt.savefig('Trayectoria.png')