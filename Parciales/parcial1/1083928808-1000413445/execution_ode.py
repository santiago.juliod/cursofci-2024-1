# Andrés Felipe Riaño Quintanilla 1083928808
# Santiago Julio Dávila 1000413445

if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt
    from classes_RK import RKG, RKG_A

    # Parámetros para el RK4
    ord = 4
    cc = [0,1/2,1/2,1]
    aa = [[1/2],[0,1/2],[0,0,1]]
    bb = [1/6,1/3,1/3,1/6]
    h = 1e-5

    # Parámetros del atractor de Lorenz
    pars = [[10,8/3,28],[10,8/3,28],[16,4,45],[16,4,45]]

    # Condiciones iniciales y finales
    ini = [[0.,np.array([0.,0.,0.,])],[0.,np.array([0.,0.01,0.,])],
           [0.,np.array([0.,0.,0.,])],[0.,np.array([0.,0.001,0.,])]]
    tf = 100

    # Soluciones del atractor de Lorenz
    for i,par in enumerate(pars):
        def lorenz(t,U,sigma=par[0],beta=par[1],rho=par[2]):
            M = np.array([[-sigma,sigma,0],[rho-U[2],-1,0],[U[1],0,-beta]])
            return M@U
        title_pars = tuple(round(p,2) for p in par)

        RK_lor = RKG_A(ord,cc,aa,bb,h,lorenz,ini[i],tf,3)
        RK_lor.soluciones_3D(['x','y','z'],f'ex2_{i}',tit=f'Atractor de Lorenz con (σ,β,ρ)={title_pars}')
